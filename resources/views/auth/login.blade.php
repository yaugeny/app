@extends('auth.single-form')

@section('form-content')

    <div class="form-group">
        <label for="email1">{{ trans('auth.email') }}</label>
        <input type="email" name="email" class="form-control" id="email1" placeholder="" value="{{ old('email') }}">
    </div>

    <div class="form-group">
        <label for="password1">{{ trans('auth.password') }}</label>
        <input type="password" name="password" class="form-control" id="password1" placeholder="">
    </div>

    <div class="checkbox">
        <label>
            <input type="checkbox" name="remember"> {{ trans('auth.remember') }}
        </label>
    </div>

    <hr/>

    <button id="submit" type="submit" class="btn btn-default">{{ trans('auth.enter') }}</button>

@endsection

@section('under-form')

    <div class="text-center">
        <a href="{{ route('password') }}" title="{{ trans('passwords.forgot') }}">{{ trans('passwords.forgot') }}</a>
    </div>

@endsection
@extends('auth.single-form')

@section('form-content')

    <div class="form-group">
        <label for="email-1">{{ trans('auth.email') }}</label>
        <input type="email" name="email" class="form-control" id="email-1" placeholder="" value="{{ old('email') }}">
    </div>

    <hr/>

    <button type="submit" class="btn btn-default">{{ trans('auth.submit') }}</button>

@endsection
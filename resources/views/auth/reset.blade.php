@extends('auth.single-form')

@section('form-content')

    <input type="hidden" name="token" value="{{ $token }}">

    <div class="form-group">
        <label for="email-1">{{ trans('auth.email') }}</label>
        <input type="email" name="email" class="form-control" id="email-1" placeholder="" value="{{ old('email') }}">
    </div>

    <div class="form-group">
        <label for="password-1">{{ trans('passwords.new') }}</label>
        <input type="password" name="password" class="form-control" id="password-1" placeholder="">
    </div>

    <div class="form-group">
        <label for="password-confirmation-1">{{ trans('auth.password_confirm') }}</label>
        <input type="password" name="password_confirmation" class="form-control" id="password-confirmation-1" placeholder="">
    </div>

    <hr/>

    <button type="submit" class="btn btn-default">{{ trans('passwords.submit') }}</button>

@endsection

@section('action', '/password/reset')
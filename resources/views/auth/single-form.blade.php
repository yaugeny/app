@extends('layouts.centered-box')

@section('content')

    <div class="flex-container">
        <div class="center-container">

            @include('partials.errors-simple')
            @include('partials.flash-simple')

            <div class="well">

                <form method="POST" action="@yield('action')">
                    {!! csrf_field() !!}

                    @yield('form-content')

                </form>

            </div>

            @yield('under-form')

        </div>
    </div>

@endsection
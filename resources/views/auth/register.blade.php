@extends('auth.single-form')

@section('head')
    <title>{{ trans('auth.register') }} | {{ config('app.name') }}</title>
@endsection

@section('form-content')

    <div class="form-group">
        <label for="name-1">{{ trans('auth.name') }}</label>
        <input type="text" name="name" class="form-control" id="name-1" placeholder="" value="{{ old('name') }}">
    </div>

    <div class="form-group">
        <label for="email-1">{{ trans('auth.email') }}</label>
        <input type="email" name="email" class="form-control" id="email-1" placeholder="" value="{{ old('email') }}">
    </div>

    <div class="form-group">
        <label for="password-1">{{ trans('auth.password') }}</label>
        <input type="password" name="password" class="form-control" id="password-1" placeholder="">
    </div>

    <div class="form-group">
        <label for="password-confirmation-1">{{ trans('auth.password_confirm') }}</label>
        <input type="password" name="password_confirmation" class="form-control" id="password-confirmation-1" placeholder="">
    </div>

    <hr/>

    <button type="submit" class="btn btn-default">{{ trans('auth.register') }}</button>

@endsection
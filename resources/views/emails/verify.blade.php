<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>trans('auth.email_validation_subject')</h2>

<div>
    Thanks for creating an account with the verification demo app.
    Please follow the link below to verify your email address
    {{ URL::to('register/verify/' . $confirmation_code) }}.<br/>

</div>

</body>
</html>
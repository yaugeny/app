<!DOCTYPE html>
<html>
<head>

    <link rel="stylesheet" href='/css/app.css'/>

    @yield('head')


</head>
<body>

    @yield('body')

    <script src='/js/app.js'></script>

</body>
</html>
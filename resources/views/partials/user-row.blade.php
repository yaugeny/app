<div class="row">
    <div class="user-row">
        <div class="container">
            <div class="row">

                @if (Auth::check())
                    <div class="col-sm-4">
                        <div class="authorize-box">
                            <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                            {{ Request::user()['name'] }}
                        </div>
                    </div>
                    <div class="col-sm-4 pull-right offset-sm-8">
                        <div class="authorize-box text-right">
                            <a href="/auth/logout">{{ trans('auth.logout') }}</a>
                        </div>
                    </div>
                @else
                    <div class="col-sm-4 pull-right offset-sm-8">
                        <div class="authorize-box text-right">
                            <a href="/auth/login">{{ trans('auth.login') }}</a>
                            •
                            <a href="/auth/register">{{ trans('auth.register') }}</a>
                        </div>
                    </div>
                @endif

            </div>
        </div>
    </div>
</div>
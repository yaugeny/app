<section class="comments-widget">

    <div class="header">

        <h2>{{ trans('comments.last') }}</h2>

    </div>
    
    <div class="articles">

        @forelse ($comments as $comment)

            <article>

                <h4><a href="{{ $comment->parent->url() }}">{{ $comment->parent->title }}</a></h4>

                <div class="subtitle">
                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span> {{ $comment->user->name }}
                </div>

                <p>{{ cocs($comment->content, 100) }}</p>

            </article>

        @empty

            {{ trans('comments.empty') }}

        @endforelse

    </div>
</section>
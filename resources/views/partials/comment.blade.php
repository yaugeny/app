<div class="row">
    <div class="comment">
        <div class="col-xs-12">
            <hr/>
            <div class="user">
                <p>
                    <span class="glyphicon glyphicon-user"></span> {{ $comment->user->name }}
                    <span class="pull-right">
                        <span class="glyphicon glyphicon-calendar"></span> {{ $comment->created_at->diffForHumans() }}
                    </span>
                </p>
            </div>
            <div class="content">
                <p>
                    {{ $comment->content }}
                </p>
            </div>
        </div>
    </div>
</div>
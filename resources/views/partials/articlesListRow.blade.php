<div class="row">
    <div class="col-xs-12">
        <article>
            <h2><a href="{{ $page->url() }}">{{ $page->title }}</a></h2>
            <div class="subtitle">
                <span class="glyphicon glyphicon-folder-open"></span>&nbsp;<a href="{{ $page->section->url() }}">{{ $page->section->name }}</a>
            </div>
            <p>{{ $page->summary }}</p>
            <hr/>
        </article>
    </div>
</div>
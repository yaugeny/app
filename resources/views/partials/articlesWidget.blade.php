<section class="articles-widget">

    <div class="header">

        <h2>{{ trans('articles.new') }}</h2>

        <div class="link">
            <a href="{{ route('articles') }}">

                {{ trans('articles.all') }} ({{ $count }})

            </a>
        </div>

    </div>

    <div class="articles">

        @forelse ($articles as $article)

            <article>

                <h4><a href="{{ $article->url() }}">{{ $article->title }}</a></h4>
                <p class="subtitle">
                    {{ trans_choice('articles.sections', 1) }}: <a href="{{ $article->section->url() }}">{{ $article->section->name }}</a>
                </p>
                <p>{{ $article->summary }}</p>

            </article>

        @empty

            {{ trans('articles.empty') }}

        @endforelse

    </div>
</section>
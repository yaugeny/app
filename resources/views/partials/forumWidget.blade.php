<section class="forum-widget">

    <div class="header">

        <h2>{{ trans('forum.new') }}</h2>

        <div class="link">
            <a href="{{ route('forum') }}">

                {{ trans('forum.forum') }}

            </a>
        </div>

    </div>

    <div class="articles">

        @forelse ($posts as $post)

            <article>

                <h4><a href="{{ $post->url() }}">{{ $post->title }}</a></h4>
                <p class="subtitle">
                    {{ trans_choice('forum.threads', 1) }}: <a href="{{ $post->thread->url() }}">{{ $post->thread->name }}</a>
                </p>

                <p>{{ cocs($post->content, 100) }}</p>

            </article>

        @empty

            {{ trans('forum.empty') }}

        @endforelse

    </div>
</section>
@extends('basic')

@section('head')
    <title>@yield('title')</title>
@endsection

@section('body')
    <div class="container-fluid">

        @include('partials.user-row')

        <div class="row">
            <div class="container">

                @yield('content')

            </div>
        </div>

    </div>
@endsection

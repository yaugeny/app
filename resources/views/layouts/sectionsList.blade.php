@extends('master')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="breadcrumbs">

                {!! $breadcrumbs !!}

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <h1>{{ $pageTitle }}</h1>
        </div>
    </div>

    @if(isset($sections))
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-heading">

                        {{ $sectionsTitle }}

                    </div>
                    <div class="panel-body">
                        <ul class="nav nav-pills">

                            @foreach($sections as $section)

                                <li><a href="{{ $section->url() }}">{{ $section->name }} <span class="badge">{{ $section->articles->count() }}</span></a></li>

                            @endforeach

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-xs-12">
            <div class="list">

                @each($listRowView, $pages, 'page')

            </div>
            {!! $pages->render() !!}
        </div>
    </div>

@endsection
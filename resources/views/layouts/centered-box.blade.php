@extends('basic')

@section('body')

    <div class="flex-container">
        <div class="center-container">

            @yield('content')

        </div>
    </div>

@endsection
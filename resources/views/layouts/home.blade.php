@extends('master')

@section('title', config('app.name'))

@section('content')

    <div class="row">
        <div class="col-sm-4">

            @include('partials.articlesWidget')

        </div>
        <div class="col-sm-4">

            @include('partials.forumWidget')

        </div>
        <div class="col-sm-4">

            @include('partials.commentsWidget')

        </div>
    </div>

@endsection

@extends('master')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="breadcrumbs">

                {!! $resource->getBreadcrumbs() !!}

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <h1>

                {{ $resource->title }}

            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="text">
                <p>

                    {{ $resource->content }}

                </p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <h2>
                {{ trans('comments.many') }} ({{ $resource->comments->count() }}):
            </h2>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-6">
            <div class="well">
                <form action="{{ '/'.Request::path().'/comment' }}" method="POST">

                    {!! csrf_field() !!}
                    {!! method_field('PUT') !!}

                    <div class="form-group">
                        <label for="comment-content">Комментировать:</label>
                        <textarea class="form-control" name="content" id="comment-content" cols="30"></textarea>
                    </div>

                    <input type="submit" value="{{ trans('misc.submit') }}"/>

                </form>
            </div>

            <div class="comments">

                @each('partials.comment', $comments, 'comment')

            </div>
        </div>
    </div>

@endsection
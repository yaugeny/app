@extends('master')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="breadcrumbs">

                {!! $breadcrumbs !!}

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <h1>{{ $pageTitle }}</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="list">

                @each($listRowView, $pages, 'page')

            </div>
            {!! $pages->render() !!}
        </div>
    </div>

@endsection
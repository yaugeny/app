<?php
return [
    'see_more'   => 'Подробнее...',
    'see_all'    => 'Просмотреть все',
    'home'       => 'Главная',
    'articles'   => 'Статьи',
    'sections'   => 'Разделы',
    'forum'      => 'Сообщество',
    'threads'    => 'Темы',
    'posts'      => 'Записи'
];

<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'failed'   => 'Неверное имя пользователя или пароль.',
    'throttle' => 'Слишком много попыток входа. Пожалуйста, попробуйте еще раз через :seconds секунд.',
    'password' => 'Пароль',
    'remember' => 'Запомнить',
    'submit' => 'Отправить',
    'email' => 'Email',
    'password_confirm' => 'Пароль еще раз',
    'name' => 'Имя',
    'login' => 'Вход',
    'enter' => 'Войти',
    'logout' => 'Выход',
    'register' => 'Регистрация',
    'email_validation_subject' => 'Подтверждение электронной почты',
    'login_unconfirmed' => 'Данный email зарегистрирован, но не подтвержден',
    'logged_in' => 'Добро пожаловать назад.',
    'email_validation_confirmed' => 'Email подтвержден, добро пожаловать.',
    'email_sent' => 'На введенный вами email выслано письмо с ссылкой для подтверждения.',
    'email_resent' => 'Пользователь зарегистрирован, но email не подтвержден. Письмо с ссылкой для активации выслано повторно. Если письма нет, оно могло попасть в спам.',
];

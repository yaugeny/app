<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('summary');
            $table->text('content');
            $table->char('slug')->unique();
            $table->boolean('active')->default(false);
            $table->integer('section_id')->unsigned();
            $table->integer('created_by')->unsigned();
            $table->timestamps();
        });

        Schema::table('articles', function (Blueprint $table) {
            $table->foreign('section_id')->references('id')->on('sections')
                  ->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('created_by')->references('id')->on('users')
                  ->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*Schema::table('articles', function(Blueprint $table) {
            $table->dropForeign('articles_section_id_foreign');
            $table->dropForeign('articles_created_at_foreign');
        });*/

        Schema::dropIfExists('articles');
    }
}

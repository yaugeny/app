<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class SuperUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'name' => 'Eugene',
            'email' => 'evgeni.konovalov@gmail.com',
            'password' => bcrypt('password'),
        ]);
    }
}

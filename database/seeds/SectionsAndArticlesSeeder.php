<?php

use App\Models\Article;
use App\Models\Comment;
use App\Models\Section;
use App\Models\Thread;
use App\Models\Post;
use Illuminate\Database\Seeder;

class SectionsAndArticlesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Filling sections with articles
         */
        factory(Section::class, 3)->create()->each(function($section){

            $section->articles()->saveMany(

                factory(Article::class, 5)->make()

            );

        });

        /*
         * Filling articles with comments
         */
        Article::all()->each(function($article){

            $article->comments()->saveMany(

                factory(Comment::class, 5)->make()

            );

        });

        /*
         * Filling threads with posts
         */
        factory(Thread::class, 2)->create()->each(function($section){

            $section->posts()->saveMany(

                factory(Post::class, 6)->make()

            );

        });

        /*
         * Filling posts with comments
         */
        Post::all()->each(function($post){

            $post->comments()->saveMany(

                factory(Comment::class, 5)->make()

            );

        });
    }
}
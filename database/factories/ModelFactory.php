<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
        'confirmed' => true,
    ];
});

$factory->define(App\Models\Section::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->sentence(random_int(2, 5)),
        'description' => $faker->realText(50),
    ];
});

$factory->define(App\Models\Article::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence(random_int(2, 5)),
        'content' => $faker->realText(500),
        'summary' => $faker->realText(100),
        'created_by' => '1',
        'active' => true,
    ];
});

$factory->define(App\Models\Thread::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->sentence(random_int(2, 5)),
        'description' => $faker->realText(50),
    ];
});

$factory->define(App\Models\Post::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence(random_int(3, 6)),
        'content' => $faker->realText(300),
        'created_by' => random_int(2, 6),
        'active' => true,
    ];
});

$factory->define(App\Models\Comment::class, function (Faker\Generator $faker) {
    return [
        'content' => $faker->sentence(random_int(10, 30)),
        'user_id' => random_int(2, 6),
    ];
});
<?php

if ( ! function_exists('cocs')) {

    /**
     * Cut On Closest Space. Cuts string on the closest to given index space (to the left).
     *
     * @param  string $str
     * @param  integer $num
     * @return string
     */
    function cocs($str = '', $index)
    {
        if (!is_null($index) && $index > 0 && $index < strlen($str)) {

            $searching = true;
            $num = $index;

            while($searching) {

                if (--$num <= 0) {
                    $num = $index;
                    break;
                }

                $searching = $str[$num] === ' ' ? false : true;

            }

            $str = str_limit($str, $num);
        }

        return $str;
    }

}
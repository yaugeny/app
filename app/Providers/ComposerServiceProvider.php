<?php

namespace App\Providers;

use App\Models\Article;
use App\Models\Comment;
use App\Models\Post;
use App\Models\Section;
use App\Models\Thread;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application view composers.
     *
     * @return void
     */
    public function boot(Request $request)
    {
        view()->composer('partials.articlesWidget', function($view) {

            $articles = Article::with('section')
                        ->active()
                        ->orderBy('created_at', 'desc')
                        ->take(5)
                        ->get();

            $view->with([
                'articles' => $articles,
                'count' => Article::all()->count(),
            ]);

        });

        view()->composer('partials.forumWidget', function($view) {

            $posts = Post::with('thread')
                ->active()
                ->orderBy('created_at', 'desc')
                ->take(5)
                ->get();

            $view->with([
                'posts' => $posts,
            ]);

        });

        view()->composer('partials.commentsWidget', function($view) {

            $comments = Comment::with(['parent', 'user'])
                ->orderBy('created_at', 'desc')
                ->take(5)
                ->get();

            $view->with([
                'comments' => $comments,
            ]);

        });

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

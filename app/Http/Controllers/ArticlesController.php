<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Section;
use Breadcrumbs;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ArticlesController extends Controller
{

    /**
     * Render article
     */
    public function getArticle($sectionSlug, $articleSlug)
    {
        $article = Article::findBySlug($articleSlug);
        $article->load('comments.user');

        return view('layouts.textPage')->with([
            'resource' => $article,
            'comments' => $article->comments->sortByDesc('created_at'),
        ]);
    }

    /**
     * Show sections page
     */
    public function getSections()
    {

        $sections = Section::with('articles')->get();
        $articles = Article::with('section')->orderBy('created_at', 'desc')->paginate(5);

        return view('layouts.sectionsList')->with([
            'sections' => $sections,
            'pages' => $articles,
            'sectionsTitle' => trans('misc.sections'),
            'pageTitle' => trans('misc.articles'),
            'breadcrumbs' => Breadcrumbs::render('articles'),
            'listRowView' => 'partials.articlesListRow',
        ]);
    }

    /**
     * Show concrete section page
     */
    public function getSection($sectionSlug)
    {

        $section = Section::findBySlug($sectionSlug);
        $articles = $section->articles()->paginate(5);

        return view('layouts.sectionsList')->with([
            'pages' => $articles,
            'pageTitle' => $section->name,
            'breadcrumbs' => Breadcrumbs::render('section', $section),
            'listRowView' => 'partials.articlesListRow',
        ]);
    }
}

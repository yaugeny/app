<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Auth\VerifiesEmail;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins, VerifiesEmail {
        VerifiesEmail::getCredentials insteadof AuthenticatesAndRegistersUsers;
        VerifiesEmail::postRegister insteadof AuthenticatesAndRegistersUsers;
        VerifiesEmail::postLogin insteadof AuthenticatesAndRegistersUsers;
    }

    // TODO: Сделать редирект на предыдущую страницу

    /**
     * Default redirect path
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $input = [
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ];

        $user = User::create($input);

        if ($this->isUsingVerifiesEmailTrait()) {

            // confirmation_code is not mass assignable parameter, so we can't use it in create() method
            $user->confirmation_code = str_random(30);
            $user->save();

            $this->sendVerificationEmailToUser($user);
        }

        // TODO: Сделать событие на создание пользователя
        // TODO: Сделать вход через соцсети (socialite)

        return $user;
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  bool  $throttles
     * @return \Illuminate\Http\Response
     */
    protected function handleUserWasAuthenticated(Request $request, $throttles)
    {
        if ($throttles) {
            $this->clearLoginAttempts($request);
        }

        flash(trans('auth.logged_in'));

        if (method_exists($this, 'authenticated')) {
            return $this->authenticated($request, Auth::user());
        }

        return redirect()->intended($this->redirectPath());
    }

    /**
     * Determine if the class is using the VerifiesEmail trait.
     *
     * @return bool
     */
    protected function isUsingVerifiesEmailTrait()
    {
        return in_array(
            VerifiesEmail::class, class_uses_recursive(get_class($this))
        );
    }
    // TODO: добавить возможность сменить почту и ее повторную верификацию (по аналогии с паролями наверное)
}

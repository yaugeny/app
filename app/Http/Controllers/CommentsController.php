<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Comment;
use Auth;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;

class CommentsController extends Controller
{
    /**
     * Create a comment for a given article.
     */
    public function putArticleComment(Request $request, $sectionSlug, $articleSlug)
    {
        $this->validate($request, [
            'content' => 'required|max:1000',
        ]);

        $data = $request->all();
        $data['user_id']     = Auth::id();
        $data['parent_id']   = Article::findBySlug($articleSlug)->id;
        $data['parent_type'] = Article::class;

        $this->create($data);

        Flash::success('Комментарий добавлен'); // TODO: Перевести

        return redirect()->back();
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Comment
     */
    protected function create(array $data)
    {
        $input = [
            'content'     => $data['content'],
            'user_id'     => $data['user_id'],
            'parent_id'   => $data['parent_id'],
            'parent_type' => $data['parent_type'],
        ];

        $comment = Comment::create($input);

        return $comment;
    }
}

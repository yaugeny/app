<?php

/*
 * Registering application breadcrumbs
 */

Breadcrumbs::register('home', function($breadcrumbs)
{
    $breadcrumbs->push(trans('misc.home'), route('home'));
});

Breadcrumbs::register('articles', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push(trans('misc.articles') , route('articles'));
});

Breadcrumbs::register('section', function($breadcrumbs, $section)
{
    $breadcrumbs->parent('articles');
    $breadcrumbs->push($section->name, route('section', $section->slug));
});

Breadcrumbs::register('article', function($breadcrumbs, $article)
{
    $breadcrumbs->parent('section', $article->section);
    $breadcrumbs->push($article->title, route('section', $article->slug));
});
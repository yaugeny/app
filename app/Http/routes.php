<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('layouts.home');
})->name('home');

// Authentication routes
Route::get('auth/login', 'Auth\AuthController@getLogin')->name('login');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister')->name('register');
Route::get('auth/register/verify/{confirmation_code}', 'Auth\AuthController@confirm');

// Password reset link request routes
Route::get('password/email', 'Auth\PasswordController@getEmail')->name('password');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::get('password/reset', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

// Articles routes
Route::get('articles/', 'ArticlesController@getSections')->name('articles');
Route::get('articles/{section}/', 'ArticlesController@getSection')->name('section');
Route::get('articles/{section}/{article}', 'ArticlesController@getArticle')->name('article');
Route::put('articles/{section}/{article}/comment', 'CommentsController@putArticleComment');

// Forum routes
Route::get('forum/', function(){})->name('forum');
Route::get('forum/{thread}', function(){})->name('thread');
Route::get('forum/{thread}/{post}', function(){})->name('post');
<?php

namespace App\Auth;

use App\Exceptions\InvalidEmailValidationException;
use App\Jobs\SendVerificationEmailToUser;
use App\Models\User;
use Auth;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Mail;

trait VerifiesEmail {

    use DispatchesJobs;

    /**
     * Handle a login request to the application.
     * Overrides same method in RegistersUsers trait
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        $this->validate($request, [
            $this->loginUsername() => 'required', 'password' => 'required',
        ]);

        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }

        // Here we make validation of confirmed status of user
        if ($user = User::where($request->only('email'))->first() and $user->confirmed == false) {

            $this->sendVerificationEmailToUser($user);

            Flash::info(trans('auth.email_resent'));

            return redirect($this->loginPath());
        }

        $credentials = $this->getCredentials($request);

        if (Auth::attempt($credentials, $request->has('remember'))) {
            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        if ($throttles) {
            $this->incrementLoginAttempts($request);
        }

        return redirect($this->loginPath())
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => $this->getFailedLoginMessage(),
            ]);
    }

    /**
     * Handle a registration request for the application.
     * Overrides same method in RegistersUsers trait
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postRegister(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $this->create($request->all());

        Flash::success(trans('auth.email_sent'));

        return redirect($this->loginPath());
    }

    /**
     * Confirms user's email
     *
     * @param string $confirmation_code
     * @return string
     */
    protected function confirm($confirmation_code)
    {
        if( ! $confirmation_code)
        {
            abort(404);
        }

        $user = User::whereConfirmationCode($confirmation_code)->first();

        if ( ! $user)
        {
            abort(404);
        }

        $user->confirmed = true;
        $user->confirmation_code = null;
        $user->save();
        // TODO: сделать эвент на прохождение верификации

        Auth::login($user);

        flash(trans('auth.email_validation_confirmed'));

        return redirect()->intended($this->redirectPath());

    }

    /**
     * Get the needed authorization credentials from the request.
     * Overrides getCredentials method from
     * Illuminate\Foundation\Auth\AuthenticatesUsers trait
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function getCredentials(Request $request)
    {
        $result = $request->only($this->loginUsername(), 'password');
        $result['confirmed'] = 1;

        return $result;
    }

    /**
     * Sends verification email to given user
     *
     * @param $user
     */
    public function sendVerificationEmailToUser($user)
    {
        $job = (new SendVerificationEmailToUser($user))->onQueue('emails');

        $this->dispatch($job);
    }
}
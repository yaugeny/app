<?php

namespace App\Models;

use Breadcrumbs;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class Article extends Model implements SluggableInterface
{
    use SluggableTrait;

    /**
     * Define field to make slug from.
     *
     * @var string
     */
    protected $sluggable = [
        'build_from' => 'title',
        'max_length' => 250,
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'articles';

    /**
     * All of the relationships to be touched.
     *
     * @var array
     */
    protected $touches = ['section'];

    /**
     * Get user that created article
     */
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * Get parent section
     */
    public function section()
    {
        return $this->belongsTo(Section::class);
    }

    /**
     * Get all comments of article
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'parent');
    }

    /**
     * Scope a query to only include active articles.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    /**
     * Get correct url for article
     *
     * @return string
     */
    public function url()
    {
        return route('article', [
            'section' => $this->section->slug,
            'article' => $this->slug
        ]);
    }

    /**
     * Render breadcrubs for article. When using inside blade template,
     * use {!! !!} tags.
     *
     * @return string
     */
    public function getBreadcrumbs() {
        return Breadcrumbs::render('article', $this);
    }
}

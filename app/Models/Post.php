<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class Post extends Model implements SluggableInterface
{
    use SluggableTrait;

    /**
     * Define field to make slug from.
     *
     * @var string
     */
    protected $sluggable = [
        'build_from' => 'title',
        'max_length' => 250,
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'posts';

    /**
     * All of the relationships to be touched.
     *
     * @var array
     */
    protected $touches = ['thread'];

    /**
     * Get user that created article
     */
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * Get parent thread
     */
    public function thread()
    {
        return $this->belongsTo(Thread::class);
    }

    /**
     * Get all comments of post
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'parent');
    }

    /**
     * Scope a query to only include active posts.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    /**
     * Get correct url for article
     */
    public function url()
    {
        return route('forum') .'/'. $this->thread->slug .'/'. $this->slug;
    }
}
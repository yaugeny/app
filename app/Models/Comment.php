<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravelrus\LocalizedCarbon\Traits\LocalizedEloquentTrait;

class Comment extends Model
{

    use LocalizedEloquentTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'comments';

    /**
     * All of the relationships to be touched.
     *
     * @var array
     */
    protected $touches = ['parent'];

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get author of comment
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get parent of comment
     */
    public function parent()
    {
        return $this->morphTo();
    }

    // TODO: Сделать url для каждого комментария (с якорем)

}

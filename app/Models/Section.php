<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class Section extends Model implements SluggableInterface
{
    use SluggableTrait;

    /**
     * Define field to make slug from.
     *
     * @var string
     */
    protected $sluggable = [
        'build_from' => 'name',
        'max_length' => 255,
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sections';

    /**
     * Get all articles in the section
     */
    public function articles() {
        return $this->hasMany(Article::class);
    }

    /**
     * Get correct url for section
     *
     * @return string
     */
    public function url()
    {
        return route('section', ['section' => $this->slug]);
    }
}

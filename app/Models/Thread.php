<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class Thread extends Model implements SluggableInterface
{
    use SluggableTrait;

    /**
     * Define field to make slug from.
     *
     * @var string
     */
    protected $sluggable = [
        'build_from' => 'name',
        'max_length' => 255,
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'threads';

    /**
     * Get all articles in the section
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    /**
     * Get correct url for section
     */
    public function url()
    {
        return route('forum') . '/' . $this->slug;
    }
}
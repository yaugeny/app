var elixir     = require('laravel-elixir'),
    gulp       = require('gulp'),
    source     = require('vinyl-source-stream'),
    browserify = require('browserify');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.js.browserify.options.debug = true; // make source map

elixir(function(mix) {
    mix
        .phpUnit()
        .sass('app.sass')
        .browserify('app.js');
});

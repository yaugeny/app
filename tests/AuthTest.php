<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\User;

class AuthTest extends TestCase
{

    use DatabaseTransactions;

    /**
     * Testing full login process
     *
     * @return void
     */
    public function testLogin()
    {

        factory(User::class)->create([
            'name'     => 'MrTester',
            'email'    => 'mr.tester@yandex.ru',
            'password' => bcrypt('MrsTester')
        ]);

        $this->visit('/')
             ->click(trans('auth.login'))
             ->seePageIs('/auth/login')
             ->type('mr.tester@yandex.ru', 'email')
             ->type('MrsTester', 'password')
             ->check('remember')
             ->press(trans('auth.enter'))
             ->seePageIs('/')
             ->see('MrTester')
             ->click(trans('auth.logout'))
             ->dontSee('MrTester');
    }

    /**
     * Testing login form errors
     *
     * @return void
     */
    public function testLoginFormErrors()
    {
        $this->visit('/auth/login')
             ->press(trans('auth.enter'))
             ->see(trans('validation.required', ['attribute' => trans('auth.password')]))
             ->see(trans('validation.required', ['attribute' => trans('auth.email')]));
    }

    /**
     * Testing login form for correct old input data display
     *
     * @return void
     */
    public function testLoginFormOldInput()
    {
        $this->visit('/auth/login')
             ->type('t@t.t', 'email')
             ->press(trans('auth.enter'))
             ->seeInField('email', 't@t.t');
    }

    /**
     * Testing full registration process
     *
     * @return void
     */
    public function testRegistration()
    {
        $tester = factory(User::class)->make([
            'password' => bcrypt('password')
        ]);

        $this->expectsJobs(App\Jobs\SendVerificationEmailToUser::class);

        $this->visit('/auth/register')
             ->type($tester->email, 'email')
             ->type($tester->name, 'name')
             ->type('password', 'password')
             ->type('password', 'password_confirmation')
             ->press(trans('auth.register'))
             ->seePageIs('auth/login')
             ->see(trans('auth.email_sent'))
             ->seeInDatabase('users', ['email' => $tester->email])
             ->visit('auth/register/verify/'.User::whereEmail($tester->email)->first()->confirmation_code)
             ->seePageIs('/')
             ->see($tester->name);
    }

    /**
     * Testing of repating verification email
     *
     * @return void
     */
    public function testRepeatVerificationEmail()
    {
        $tester = factory(User::class)->make([
            'password' => bcrypt('password')
        ]);

        $this->expectsJobs(App\Jobs\SendVerificationEmailToUser::class);

        $this->visit('/auth/register')
             ->type($tester->email, 'email')
             ->type($tester->name, 'name')
             ->type('password', 'password')
             ->type('password', 'password_confirmation')
             ->press(trans('auth.register'))
             ->seePageIs('auth/login')
             ->see(trans('auth.email_sent'))
             ->visit('/auth/login') // Without this it works wrong...
             ->type($tester->email, 'email')
             ->type('password', 'password')
             ->press(trans('auth.enter'))
             ->see(trans('auth.email_resent'));
    }

    /**
     * Testing registration form for correct errors display
     *
     * @return void
     */
    public function testRegisterFormErrors()
    {
        $this->visit('/auth/register')
             ->press(trans('auth.register'))
             ->see(trans('validation.required', ['attribute' => trans('auth.password')]))
             ->see(trans('validation.required', ['attribute' => trans('auth.email')]))
             ->see(trans('validation.required', ['attribute' => trans('auth.name')]));
    }

    /**
     * Testing registration form for correct old input data display
     *
     * @return void
     */
    public function testRegisterFormOldInput()
    {
        $this->visit('/auth/register')
            ->type('t@t.t', 'email')
            ->type('Mr.Pink', 'name')
            ->press(trans('auth.register'))
            ->seeInField('email', 't@t.t')
            ->seeInField('name', 'Mr.Pink');
    }

}

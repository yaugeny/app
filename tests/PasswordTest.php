<?php

use App\Models\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PasswordTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test for password reset
     *
     * @return void
     */
    public function testPasswordReset()
    {
        $tester = factory(User::class)->create([
            'password' => bcrypt('password')
        ]);

        $this->visit('/')
             ->click(trans('auth.login'))
             ->click(trans('passwords.forgot'))
             ->seePageIs('password/email')
             ->type($tester->email, 'email')
             ->press(trans('auth.submit'))
             ->see(trans('passwords.sent'))
             ->seeInDatabase('password_resets', ['email' => $tester->email])
             ->visit('password/reset/' . DB::table('password_resets')->where('email', $tester->email)->value('token'))
             ->type($tester->email, 'email')
             ->type('newpassword', 'password')
             ->type('newpassword', 'password_confirmation')
             ->press('passwords.submit')
             ->visit('auth/logout')
             ->visit('auth/login')
             ->type($tester->email, 'email')
             ->type('newpassword', 'password')
             ->press(trans('auth.enter'))
             ->dontSeeLink(trans('auth.login'));
    }

    /**
     * Test password reset mechanism for wrong email given
     *
     * @return void
     */
    public function testWrongEmailGiven()
    {
        $this->visit('password/email')
             ->type('fake@mai.l', 'email')
             ->press(trans('auth.submit'))
             ->seeInField('email', 'fake@mai.l')
             ->see(trans('passwords.user'));
    }

    /**
     * Test password reset mechanism for wrong token used
     *
     * @return void
     */
    public function testWrongTokenUsed()
    {
        $this->get('password/reset')
             ->seeStatusCode('404');

        $this->get('password/reset/wrongtoken')
             ->seeStatusCode('404');
    }

    /**
     * Test password reset form for correct error handling
     *
     * @return void
     */
    public function testResetFormErrors()
    {
        DB::table('password_resets')->insert(['email' => 'test@test.test', 'token' => 'testingtoken', 'created_at' => time()]);

        $this->visit('password/reset/testingtoken')
             ->press(trans('passwords.submit'))
             ->see(trans('validation.required', ['attribute' => trans('auth.password')]))
             ->see(trans('validation.required', ['attribute' => trans('auth.email')]));

    }

    /**
     * Test password reset form for correct old input viewing
     *
     * @return void
     */
    public function testResetFormOldInput()
    {
        DB::table('password_resets')->insert(['email' => 'test@test.test', 'token' => 'testingtoken', 'created_at' => time()]);

        $this->visit('password/reset/testingtoken')
             ->type('test@test.test', 'email')
             ->press(trans('passwords.submit'))
             ->seeInField('email', 'test@test.test');
    }
}
